import React,{useState, createContext} from 'react';
import MovieList from './MovieList';
import { tsPropertySignature } from '@babel/types';

export const MovieContext = createContext();

export const MovieProvider = props => {
    const [movies, setMovies] = useState([
        {
            name: 'Test1',
            price: '10$',
            id: 23124
        },
        {
            name: 'Test2',
            price: '15$',
            id: 2566124
        },
        {
            name: 'Test3',
            price: '9$',
            id: 23524
        }
    ]);
    return(
        <MovieContext.Provider value={[movies, setMovies]}>
            {props.children}
        </MovieContext.Provider>
    );
}