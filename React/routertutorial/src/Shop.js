import React, {useState, useEffect} from 'react';
import './App.css';

function Shop() {
  useEffect(() => {
    fetchItems();
  }, []);

  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    const data = await fetch(
      'http://fortnite-public-api.theapinetwork.com/prod09/upcoming/get'
    );

    const items = await
    console.log(data);
  }

  return (
    <div>
      <h1>Shop Page</h1>
    </div>
  );
}

export default Shop;
