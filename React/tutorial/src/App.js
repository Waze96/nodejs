import React, {useState} from 'react';
import Tweet from './Tweet';

function App() {

  const [isRed, setRed] = useState(false);
  const [count, setCount] = useState(0);

  const [user, setUser] = useState([
    {name:'Edu', message:'Test1'},
    {name:'Edu2', message:'Test2'},
    {name:'Edu3', message:'Test3'},
  ]);

  const increment = () => {
      setCount(count +1);
      //Switch between true/false
      setRed(!isRed);
  };

  return (
    <div className='App'>
        {/* if is true (?) the class 'red' will be added, else(:) no add anything*/}
        <h1 className={isRed ? 'red' : ""}>Change my color!</h1>
        <button onClick={increment}>Increment</button>
        <h1>{count}</h1>
        {user.map(user =>(
            <Tweet name={user.name} message={user.message}/>
        ))}
    </div>
  );
}

export default App;
